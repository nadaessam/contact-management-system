from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)


class Contact(models.Model):

    _name = 'contact.management.system.contact'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _inherits = {'res.partner': 'partner_id'}

    @api.multi
    def send_greeting_mail(self):
        template = self.env.ref('contact_managment_system.example_email_template')
        self.env['mail.template'].browse(template.id).send_mail(self.id, force_send=True)














