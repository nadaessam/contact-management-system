from django.db import models

class Contact(models.Model):
    name = models.CharField(max_length = 100)
    city = models.CharField(max_length = 50)
    street = models.CharField(max_length = 100)
    street2 = models.CharField(max_length = 100)
    mobile = models.CharField(max_length = 50)
    phone = models.CharField(max_length = 50)
    date_of_birth = models.DateField()
    email = models.CharField(max_length = 50)

    def __str__(self):
        return self.name
