from django.shortcuts import render
from .models import Contact
from .forms import ContactForm
from django.http import HttpResponseRedirect
import xmlrpc.client

# Configuration
url = 'http://localhost:8069'
db = 'contact_management_system'
username = 'nessam378@gmail.com'
password = 'nada'

# logging in
common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})

# Calling methods
models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))



def index(request):
	contacts = Contact.objects.all()
	context = {'all_cont': contacts}
	return render(request, 'contact/index.html', context)

def singlepage(request, contact_id):
	cont = Contact.objects.get(id=contact_id)
	context = {'s_cont': cont}
	return render(request, 'contact/singlepage.html', context)

def contact_new(request):
	form = ContactForm()
	if request.method == "POST":
		form = ContactForm(request.POST)
		if form.is_valid():
			form.save()
			id = models.execute_kw(db, uid, password, 'contact.management.system.contact', 'create', [{
				'name': request.POST['name'],
				'city': request.POST['city'],
				'street': request.POST['street'],
				'street2': request.POST['street2'],
				'mobile': request.POST['mobile'],
				'phone': request.POST['phone'],
				'date_of_birth': request.POST['date_of_birth'],
				'email': request.POST['email'],
			}])
			return HttpResponseRedirect('/')
	return render(request, 'contact/new.html', {'form':form})

def contact_edit( request, contact_id):
	cont = Contact.objects.get(id=contact_id)
	if request.method == "POST":
		form = ContactForm( request.POST, instance=cont)
		if form.is_valid():
			form.save()
			models.execute_kw(db, uid, password, 'contact.management.system.contact', 'write', [[contact_id], {
				'name': request.POST['name'],
				'city': request.POST['city'],
				'street': request.POST['street'],
				'street2': request.POST['street2'],
				'mobile': request.POST['mobile'],
				'phone': request.POST['phone'],
				'date_of_birth': request.POST['date_of_birth'],
				'email': request.POST['email'],
			}])
			return HttpResponseRedirect('/')
	else:
		form = ContactForm( instance=cont)
	return render(request, 'contact/new.html', {'form':form})


def contact_delete(request, contact_id):
	obj = Contact.objects.get(id=contact_id)
	obj.delete()
	models.execute_kw(db, uid, password, 'contact.management.system.contact', 'unlink', [[contact_id]])
	return HttpResponseRedirect('/')