from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', views.index, name='index' ),
	url(r'^contact/(?P<contact_id>[0-9]+)/$', views.singlepage, name='singlepage' ),
	url(r'^contact/new/$', views.contact_new, name='contact_new' ),
	url(r'^contact/(?P<contact_id>[0-9]+)/edit/$', views.contact_edit, name='edit' ),
	url(r'^contact/(?P<contact_id>[0-9]+)/del/$', views.contact_delete, name='delete'),
]
