from django import forms 
from django.forms import ModelForm, DateInput
from .models import Contact 

class ContactForm(forms.ModelForm):
	class Meta:
		model = Contact
		fields = ('name', 'city', 'street', 'street2', 'mobile', 'phone', 'date_of_birth', 'email')

		widgets = {
		'name': forms.TextInput( attrs={'class': 'form-control '}),
		'city' : forms.TextInput( attrs={'class': 'form-control '}),
		'street': forms.TextInput( attrs={'class': 'form-control '}),
		'street2': forms.TextInput( attrs={'class': 'form-control '}),
		'mobile': forms.TextInput( attrs={'class': 'form-control '}),
		'phone': forms.TextInput( attrs={'class': 'form-control '}),
		'date_of_birth': forms.DateInput( format='%m/%d/%Y', attrs={'class': 'form-control datepicker'}),
		'email': forms.EmailInput( attrs={'class': 'form-control '})
		}
			
